""" this program generates a password so
         random of a requested length and containing
         uppercase, lowercase, number and special character on
         base of the ASCII table"""
from Crypto.Random import random


#random choice of a character from the bounded ASCII table
def aleatoire(inf, sup):
    caract = chr(random.randint(inf, sup))
    return caract

def princ():
    long =int( input("length of  psw : "))
    pasword = ""
    for i in range(long):
        choix = random.randint(0, 3)
        if choix == 0:  # lowercase
            deb, fin = 97, 122
            
        elif choix == 1: #uppercase
            deb, fin = 65, 90
            
        elif choix == 2 : # number
            deb, fin = 48, 57
            
        else : # special character
            deb, fin = 35, 38
            
        pasword = pasword + aleatoire(deb, fin)
        
    return pasword

if __name__ == '__main__':
    print (princ())